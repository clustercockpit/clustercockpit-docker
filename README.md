# General Information

- just tested on CentOS7

- access to web app only via localhost

- Uses ClusterCockpit via submodule

# Build Steps

## 1. Install and start Docker and Docker-Compose:

Follow for installation: https://docs.docker.com/engine/install/centos/

`sudo systemctl enable docker && sudo systemctl start docker`

## 2. Clone CC Docker Repository

`git clone https://gitlab.dkrz.de/clustercockpit/clustercockpit-docker.git && cd clustercockpit-docker`

## 3. Create secrets

- secrets/mysql_root.secret
- secrets/mysql_cockpit.secret
- secrets/cockpit_password.secret
- secrets/cockpit.env

A sample for the `cockpit.env` is provided. Everything in the `*secret` files is interpreted as a password.

## 4. Initialize submodule

`git submodule update --init`

## 5. Build and start container
 
`sudo docker-compose up`

# Access

Access in Browser via localhost:8000
